<?php

/**
 * @file
 * Contains \Drupal\entity_collection_access\EntityCollectionAccessControlHandlerTrait.
 */

namespace Drupal\entity_collection_access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Defines a default implementation for entity access control handler.
 */
trait EntityCollectionAccessControlHandlerTrait {

  /**
   * {@inheritdoc}
   */
  public function collectionAccess($entity_bundle = NULL, AccountInterface $account = NULL, array $context = array(), $return_as_object = FALSE) {
    $account = $this->prepareUser($account);
    $context += array(
      'langcode' => LanguageInterface::LANGCODE_DEFAULT,
    );

    $cid = $entity_bundle ? 'collection:' . $entity_bundle : 'collection';
    if (($access = $this->getCache($cid, 'collection', $context['langcode'], $account)) !== NULL) {
      // Cache hit, no work necessary.
      return $return_as_object ? $access : $access->isAllowed();
    }

    // Invoke hook_entity_create_access() and hook_ENTITY_TYPE_create_access().
    // Hook results take precedence over overridden implementations of
    // EntityAccessControlHandler::checkAccess(). Entities that have checks that
    // need to be done before the hook is invoked should do so by overriding
    // this method.
    // We grant access to the entity if both of these conditions are met:
    // - No modules say to deny access.
    // - At least one module says to grant access.
    $access = array_merge(
      $this->moduleHandler()->invokeAll('entity_collection_access', array($account, $context, $entity_bundle)), $this->moduleHandler()->invokeAll($this->entityTypeId() . '_collection_access', array($account, $context, $entity_bundle))
    );

    $return = $this->processAccessHookResults($access);

    // Also execute the default access check except when the access result is
    // already forbidden, as in that case, it can not be anything else.
    if (!$return->isForbidden()) {
      $return = $return->orIf($this->checkCollectionAccess($account, $context, $entity_bundle));
    }
    $result = $this->setCache($return, $cid, 'collection', $context['langcode'], $account);
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * Performs collection access checks.
   *
   * This method is supposed to be overwritten by extending classes that
   * do their own custom access checking.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user for which to check access.
   * @param array $context
   *   An array of key-value pairs to pass additional context when needed.
   * @param string|null $entity_bundle
   *   (optional) The bundle of the entity. Required if the entity supports
   *   bundles, defaults to NULL otherwise.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function checkCollectionAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    if ($admin_permission = $this->entityType()->getAdminPermission()) {
      return AccessResult::allowedIfHasPermission($account, $admin_permission);
    }
    else {
      // No opinion.
      return AccessResult::neutral();
    }
  }

  /**
   * Tries to retrieve a previously cached access value from the static cache.
   *
   * @param string $cid
   *   Unique string identifier for the entity/operation, for example the
   *   entity UUID or a custom string.
   * @param string $operation
   *   The entity operation. Usually one of 'view', 'update', 'create' or
   *   'delete'.
   * @param string $langcode
   *   The language code for which to check access.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user for which to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface|null
   *   The cached AccessResult, or NULL if there is no record for the given
   *   user, operation, langcode and entity in the cache.
   */
  abstract protected function getCache($cid, $operation, $langcode, AccountInterface $account);

  /**
   * Statically caches whether the given user has access.
   *
   * @param \Drupal\Core\Access\AccessResultInterface $access
   *   The access result.
   * @param string $cid
   *   Unique string identifier for the entity/operation, for example the
   *   entity UUID or a custom string.
   * @param string $operation
   *   The entity operation. Usually one of 'view', 'update', 'create' or
   *   'delete'.
   * @param string $langcode
   *   The language code for which to check access.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user for which to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Whether the user has access, plus cacheability metadata.
   */
  abstract protected function setCache($access, $cid, $operation, $langcode, AccountInterface $account);

  /**
   * Clears all cached access checks.
   */
  abstract public function resetCache();

  /**
   * Returns the module handler.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler.
   */
  abstract protected function moduleHandler();

  /**
   * We grant access to the entity if both of these conditions are met:
   * - No modules say to deny access.
   * - At least one module says to grant access.
   *
   * @param \Drupal\Core\Access\AccessResultInterface[] $access
   *   An array of access results of the fired access hook.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The combined result of the various access checks' results. All their
   *   cacheability metadata is merged as well.
   *
   * @see \Drupal\Core\Access\AccessResultInterface::orIf()
   */
  abstract protected function processAccessHookResults(array $access);

  /**
   * The entity type ID of the access control handler instance.
   *
   * @return string
   */
  protected function entityTypeId() {
    return $this->entityType()->id();
  }

  /**
   * Information about the entity type.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   */
  abstract protected function entityType();
}

<?php

/*
 * @file
 * Contains \Drupal\entity_collection_access\EntityCollectionAccessCheck.
 */

namespace Drupal\entity_collection_access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Defines an access checker for entity collection.
 */
class EntityCollectionAccessCheck implements AccessInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The key used by the routing requirement.
   *
   * @var string
   */
  protected $requirementsKey = '_entity_collection_access';

  /**
   * Constructs a EntityCollectionAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * Checks access to collection the entity type and bundle for the given route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    list($entity_type, $bundle) = explode(':', $route->getRequirement($this->requirementsKey) . ':');

    // The bundle argument can contain request argument placeholders like
    // {name}, loop over the raw variables and attempt to replace them in the
    // bundle name. If a placeholder does not exist, it won't get replaced.
    if ($bundle && strpos($bundle, '{') !== FALSE) {
      foreach ($route_match->getRawParameters()->all() as $name => $value) {
        $bundle = str_replace('{' . $name . '}', $value, $bundle);
      }
      // If we were unable to replace all placeholders, deny access.
      if (strpos($bundle, '{') !== FALSE) {
        return AccessResult::neutral();
      }
    }
    if (empty($bundle)) {
      $bundle = NULL;
    }

    $access_control_handler = $this->entityManager->getAccessControlHandler($entity_type);
    if (!($access_control_handler instanceof EntityCollectionAccessControlHandlerInterface)) {
      return AccessResult::neutral();
    }

    return $access_control_handler->collectionAccess($bundle, $account, [], TRUE);
  }

}
